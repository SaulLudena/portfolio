import './assets/styles/Scss/main/main.css';
import React from 'react';
import Menu from './components/menu/menu';
import Presentation from './components/presentation/presentation';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
function App() {
  return (
    <Router>
      <div className='layout'>
      <Menu/>
        <Routes>
            <Route path="/" element={<Presentation/>}/>
            <Route path="/xd" element={<Menu/>}/>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
