import React from 'react'
import './menu.css';

export default function menu() {
  return (
    <nav>
        <div className="logo-container">
           <a href="">WD</a>
        </div>
        <div className="menu-container">
                <ul className="links">
                    <li className="link-item">
                        <a href="">
                            <i className="fa-solid fa-house-chimney"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li className="link-item">
                        <a href="">
                             <i className="fa-solid fa-briefcase"></i>
                            <span>Portfolio</span>
                        </a>
                    </li>
                    <li className="link-item">
                        <a href="">
                            <i className="fa-solid fa-brain"></i>
                            <span>About</span>    
                        </a>
                    </li>
                    <li className="link-item">
                        <a href="">
                            <i className="fa-solid fa-address-card"></i>
                            <span>Contact</span>  
                        </a>
                    </li>

                </ul>
        </div>

    </nav>
  )
}
