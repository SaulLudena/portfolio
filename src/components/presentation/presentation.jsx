import React from 'react'
import './presentation.css';
export default function presentation() {
  return (
    <div className="presentation-container">
        <div className="presentation-content">
            <h3>Hello, beautiful people</h3>
            <span>
                <h1>I'm</h1> 
                <h1>Wilson</h1>
            </span>
            <p>A web developer based in Peru, i enjoy learn things about software <br/> development</p>
            <div className="button-container">
                <a href="" className="button">
                    Get in touch
                </a>
            </div>
            <ul className="links">
                <li className="link-item"><a href=""><i class="fa-brands fa-github"></i></a></li>
                <li className="link-item"><a href=""><i class="fa-brands fa-linkedin-in"></i></a></li>
            </ul>
        </div>
    </div>
  )
}
